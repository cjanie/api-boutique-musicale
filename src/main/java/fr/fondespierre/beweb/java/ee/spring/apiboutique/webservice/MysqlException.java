package fr.fondespierre.beweb.java.ee.spring.apiboutique.webservice;

public class MysqlException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MysqlException() {
		super("Mysql server error");
	}

}
