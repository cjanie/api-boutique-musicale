package fr.fondespierre.beweb.java.ee.spring.apiboutique.webservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.fondespierre.beweb.java.ee.spring.apiboutique.entities.Item;

@RestController
@RequestMapping("/rest/items")
@CrossOrigin(origins = "*")
public class ItemController {
	
	@Autowired
	private ItemService itemService;

	@GetMapping
	public ResponseEntity<?> listItems() {
		List<Item> items = null;
		try {
			items = this.itemService.list();
		} catch (MysqlException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<Item>>(items, HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<?> getItem(@PathVariable("id") Long id) {
		Item item = null;
		try {
			item = this.itemService.get(id);
		} catch (MysqlException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Item>(item, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<?> addItem(@RequestBody Item item) {
		Long id = 0L;
		try {
			id = this.itemService.add(item);
		} catch (MysqlException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Long>(id, HttpStatus.CREATED);
	}
	
	@PutMapping(value = "/{id}")
	public ResponseEntity<?> updateItem(@PathVariable("id") Long id, @RequestBody Item item) {
		try {
			this.itemService.update(item);
		} catch (MysqlException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<?> deleteItem(@PathVariable("id") Long id) {
		try {
			this.itemService.delete(id);
		} catch (MysqlException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
}
