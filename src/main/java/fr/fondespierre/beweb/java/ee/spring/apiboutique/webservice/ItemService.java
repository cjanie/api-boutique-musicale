package fr.fondespierre.beweb.java.ee.spring.apiboutique.webservice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.fondespierre.beweb.java.ee.spring.apiboutique.entities.Item;

@Service
public class ItemService {
	
	@Autowired
	private ItemRepository itemRepository;
	
	public List<Item> list() throws MysqlException {
		List<Item> items = new ArrayList<>();
		this.itemRepository.findAll().forEach(item -> {
			items.add(item);
		});
		return items;
	}
	
	public Item get(Long id) throws MysqlException {
		return this.itemRepository.findById(id).get();
	}
	
	public Long add(Item item) throws MysqlException {
		return this.itemRepository.save(item).getId();
	}
	
	public void update(Item item) throws MysqlException {
		this.itemRepository.save(item);
	}
	
	public void delete(Long id) throws MysqlException {
		this.itemRepository.deleteById(id);
	}

}
