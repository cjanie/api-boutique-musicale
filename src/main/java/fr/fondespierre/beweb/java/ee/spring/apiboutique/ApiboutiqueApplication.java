package fr.fondespierre.beweb.java.ee.spring.apiboutique;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiboutiqueApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiboutiqueApplication.class, args);
	}

}
