package fr.fondespierre.beweb.java.ee.spring.apiboutique.webservice;

import org.springframework.data.repository.CrudRepository;

import fr.fondespierre.beweb.java.ee.spring.apiboutique.entities.Item;

public interface ItemRepository extends CrudRepository<Item, Long> {

}
